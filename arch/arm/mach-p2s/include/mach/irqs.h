#ifndef __ASM_ARCH_IRQS_H
#define __ASM_ARCH_IRQS_H

#define NR_IRQS				32

//interrupt mapping on page 46
#define IRQ_UART0  0
#define IRQ_UART1  1
#define IRQ_UART2  2
#define IRQ_TIMER_012 3


#endif//!__ASM_ARCH_IRQS_H
