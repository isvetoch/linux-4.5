/*
 *  Setup code for P2S0927A2
 *
 *  Copyright (C) 2016 MDB Compas
 *
 *	Andrew Gazizov   <gazizovandrey@live.ru>
 *  				 <a.gazizov@mkb-kompas.ru>
 *
 * Licensed under GPLv2 or later.
 */
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <asm/mach/arch.h>

#define P2S_MSCU_CID 	0x00

static void __init p2s0927a2_dt_device_init(void)
{
	struct device_node *np;
	void __iomem *regs;
	u32 cid;

	np = of_find_compatible_node(NULL, NULL, "compas,p2s0927a2-mscu");
	if (!np)
		pr_warn("Could not find MSCU node");

	regs = of_iomap(np, 0);
	if (!regs)
		pr_warn("Could not map MSCU iomem range");

	cid = readl(regs + P2S_MSCU_CID);

	iounmap(regs);

	pr_info("Detected SoC: p2s0927a2, chip ID %X\n", cid);

	of_platform_populate(NULL, of_default_bus_match_table, NULL, NULL);
}

static const char *const p2s0927a2_dt_board_compat[] __initconst = {
	"compas,p2s0927a2",
	NULL
};

DT_MACHINE_START(p2s0927a2, "Compas P2S0927A2")
	.atag_offset	= 0x100,
	.init_machine	= p2s0927a2_dt_device_init,
	.dt_compat		= p2s0927a2_dt_board_compat,
MACHINE_END
