/*
 * include/linux/clk/p2s_mscu.h
 *
 *  Copyright (C) 2016 MDB Compas
 *
 *	Andrew Gazizov   <gazizovandrey@live.ru>
 *
 * Master System Controller Unit (MSCU) - System controller registers.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef P2S_MSCU_H
#define P2S_MSCU_H

#ifndef __ASSEMBLY__
extern void __iomem *p2s_mscu_base;

#define p2s_mscu_read(field) \
	readl_relaxed(p2s_mscu_base + field)

#define p2s_mscu_write(field, value) \
	writel_relaxed(value, p2s_mscu_base + field)
#else
.extern p2s_mscu_base
#endif


#define P2S_MSCU_PLL1CON		0x04	/* PLL1 control register */
#define P2S_MSCU_PLL2CON		0x08	/* PLL2 control register */

#define P2S_MSCU_CLKCFG			0x0C	/* Clock gating register */
#define P2S_MSCU_SDT			BIT(0)		/* SDT/ST Memory Ctrl. Clock Disable */
#define P2S_MSCU_BOOT			BIT(1)		/* BOOT ROM AHB Clock Disable */
#define P2S_MSCU_SRAM			BIT(2)		/* SRAM32KB AHB Clock Disable */
#define P2S_MSCU_DPSRAM			BIT(3)		/* DP-SRAM8KB AHB Clock Disable */
#define P2S_MSCU_HDMA0			BIT(4)		/* HDMA0 AHB Clock Disable */
#define P2S_MSCU_HDMA1			BIT(5)		/* HDMA1 AHB Clock Disable */
#define P2S_MSCU_SDHC			BIT(6)		/* SD Host Ctrl. AHB Clock Disable */
#define P2S_MSCU_MAC			BIT(7)		/* MAC AHB Clock Disable */
#define P2S_MSCU_LCD			BIT(8)		/* LCD Ctrl. AHB Clock Disable */
#define P2S_MSCU_OTG			BIT(9)		/* OTG Ctrl. AHB Clock Disable */
#define P2S_MSCU_MDDR			BIT(10)		/* mDDR Ctrl. AHB Clock Disable */
#define P2S_MSCU_NAND			BIT(11)		/* NAND Flash Ctrl. AHB Clock Disable */
#define P2S_MSCU_TIMER			BIT(12)		/* Timer APB Clock Disable */
#define P2S_MSCU_RTC			BIT(13)		/* RTC APB Clock Disable */
#define P2S_MSCU_WDT			BIT(14)		/* WDT APB Clock Disable */
#define P2S_MSCU_GPIO			BIT(15)		/* GPIO APB Clock Disable */
#define P2S_MSCU_SSP0			BIT(16)		/* SSP0 APB Clock Disable */
#define P2S_MSCU_SSP1			BIT(17)		/* SSP1 APB Clock Disable */
#define P2S_MSCU_SSP2			BIT(18)		/* SSP2 APB Clock Disable */
#define P2S_MSCU_SSP3			BIT(19)		/* SSP3 APB Clock Disable */
#define P2S_MSCU_I2C0			BIT(20)		/* I2C0 APB Clock Disable */
#define P2S_MSCU_I2C1			BIT(21)		/* I2C1 APB Clock Disable */
#define P2S_MSCU_I2S			BIT(22)		/* I2S APB Clock Disable */
#define P2S_MSCU_UART0			BIT(23)		/* UART0 APB Clock Disable */
#define P2S_MSCU_UART1			BIT(24)		/* UART1 APB Clock Disable */
#define P2S_MSCU_UART2			BIT(25)		/* UART2 APB Clock Disable */
#define P2S_MSCU_PWM			BIT(26)		/* PWM APB Clock Disable */
#define P2S_MSCU_CAN			BIT(27)		/* CAN APB Clock Disable */
#define P2S_MSCU_KEYPAD			BIT(28)		/* KeyPAD APB Clock Disable */
#define P2S_MSCU_ADC			BIT(29)		/* ADC Ctrl. APB Clock Disable */


#define P2S_MSCU_ACLKCFG		0x10	/* Application clock gating register */
#define P2S_MSCU_CLKDIV			0x14	/* System clock divider control register */
#define P2S_MSCU_REMAP			0x18	/* Decoder remap control register */

#define P2S_MSCU_PWMCON			0x1C	/* Power management register */
#define P2S_MSCU_NORMAL_MODE	BIT(0)		/* Normal/Slow mode selection */

#define P2S_MSCU_SWRST			0x20	/* Software reset register */
#define P2S_MSCU_SWRST_SLAVE	0x24	/* Slave System Software reset register */
#define P2S_MSCU_CPUCFG			0x28	/* ARM926 Control register */
#define P2S_MSCU_BOOTSTATUS		0x2C	/* System BOOT Mode Status register  */

#define P2S_MSCU_GPIOCFG		0x30	/* GPIO Pin Sharing Control register */
#define P2S_MSCU_MUX_MAC		BIT(0)	/* MAC pin sharing selection */
#define P2S_MSCU_MUX_UART2		BIT(1)	/* UART2 pin sharing selection */
#define P2S_MSCU_MUX_I2S		BIT(2)	/* I2S pin sharing selection */
#define P2S_MSCU_MUX_CAN		BIT(3)	/* CAN pin sharing selection */

#define P2S_MSCU_RSTCFG			0x40	/* SW Reset register */

#endif
