/*
 *  Header file for Compas LCD Controller
 *
 *  Data structure and register user interface
 *
 * (C) Copyright 2016 MDB "Compas"
 * Author: Andrew Gazizov, MDB "Compas", <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __COMPAS_LCDC_H__
#define __COMPAS_LCDC_H__

 /* LCD Controller info data structure, stored in device platform_data */
struct compas_lcdfb_pdata {
	u8			default_bpp;
	struct fb_monspecs	*default_monspecs;
};

#define COMPAS_LCDC_CTRL			0x00	/* LCD Control Register */
#define COMPAS_LCDC_ENABLE			BIT(0)		/* LCDC Enable */
#define COMPAS_LCDC_RGB_HALT		BIT(1)		/* RGB halt */
#define COMPAS_LCDC_AC_INV			BIT(2)		/* LCD_AC level */
#define COMPAS_LCDC_VSYNC_INV		BIT(3)		/* vsync level */
#define COMPAS_LCDC_HSYNC_INV		BIT(4)		/* hsync level */
#define COMPAS_LCDC_PAGE_SWAP		BIT(5)		/* Page frame swap */
#define COMPAS_LCDC_LUT_ENABLE		BIT(6)		/* Colour LUT enable */
#define COMPAS_LCDC_24BPP			BIT(7)		/* Colour depth to output */
#define COMPAS_LCDC_PCLOCK(x)		((x & 0xFF) << 8)  /* Pixel clock
															divisor output */
#define COMPAS_LCDC_PCLOCK_MASK		GENMASK(15,8)
#define COMPAS_LCDC_LUMCONFIG(x)	((x) << 17)	/* Luminance configuration */
#define COMPAS_LCDC_LUMCONFIG_RGB	(BIT(0) | BIT(1) | BIT(2))
#define COMPAS_LCDC_LUMCONFIG_B		BIT(0)
#define COMPAS_LCDC_LUMCONFIG_G		BIT(1)
#define COMPAS_LCDC_LUMCONFIG_R		BIT(2)
#define COMPAS_LCDC_COLOUR(x)		((x) << 20)	/* Colour sensitivity */
#define COMPAS_LCDC_COLOUR_RED		1
#define COMPAS_LCDC_COLOUR_GREEN	2
#define COMPAS_LCDC_COLOUR_BLUE		3
#define COMPAS_LCDC_PXCLK_RISING	BIT(22)		/* Pixel Clock Polarity */

#define COMPAS_LCDC_YUV2RGB_CTRL	0x08	/* LCD Control register for
													YUV to RGB convert */

#define COMPAS_LCDC_H_TIMING		0x10	/* LCD Horizontal Timing */
#define COMPAS_LCDC_V_TIMING		0x14	/* LCD Vertical Timing */
#define COMPAS_LCDC_ACTIVE(x)		((x & 0x3FF) << 0) 	/* Active */
#define COMPAS_LCDC_RESET_SYNC(x)	((x & 0x03F) << 10) /* Reset sync */
#define COMPAS_LCDC_BACK_PORCH(x)	((x & 0x0FF) << 16) /* Back porch */
#define COMPAS_LCDC_FRONT_PORCH(x)	((x & 0x0FF) << 24) /* Front porch */

#define COMPAS_LCDC_LUT_ADDR		0x1C	/* Colour LUT Memory Address */
#define COMPAS_LCDC_PAGE0_ADDR		0x20	/* LCD Memory Page 0 Address */
#define COMPAS_LCDC_PAGE1_ADDR		0x24	/* LCD Memory Page 1 Address */
#define COMPAS_LCDC_ENDIAN			0x30	/* LCD Data Format Endian Set */
#define COMPAS_LCDC_INTR_STS		0x40	/* LCD Interrupt Status */
#define COMPAS_LCDC_INTR_EN			0x44	/* LCD Interrupt enable */
#define COMPAS_LCDC_INTR_DIS		0x48	/* LCD Interrupt disable */
#define COMPAS_LCDC_INTR_MASK		0x4C	/* LCD Interrupt Mask */
/* LCD Memory Page 0 */
#define COMPAS_LCDC_Y_PAGE0_ADDR	0x50	/* Y component address */
#define COMPAS_LCDC_U_PAGE0_ADDR	0x54	/* U component address */
#define COMPAS_LCDC_V_PAGE0_ADDR	0x58	/* V component address */
/* LCD Memory Page 1 */
#define COMPAS_LCDC_Y_PAGE1_ADDR	0x60	/* Y component address */
#define COMPAS_LCDC_U_PAGE1_ADDR	0x64	/* U component address */
#define COMPAS_LCDC_V_PAGE1_ADDR	0x68	/* V component address */

#endif /* __COMPAS_LCDC_H__ */
