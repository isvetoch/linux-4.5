#ifndef __ASM_ARCH_P2S_H
#define __ASM_ARCH_P2S_H

#define IO_ADDRESS_INT(x) (((x) | 0xf0000000) - 0x01000000)
#define IO_ADDRESS_PTR(x) IOMEM(((x) | 0xf0000000) - 0x01000000)

#define P2S_AHB0_INTC  0x18004000
#define P2S_APB0_UART0 0x1902C000
#define P2S_APB0_UART1 0x19030000
#define P2S_APB0_UART2 0x19034000
#define P2S_APB0_TIMER 0x19000000
#define P2S_APB0_GPIO0 0x1900C000
#define P2S_APB0_WDT   0x19008000

#endif//!__ASM_ARCH_P2S_H
