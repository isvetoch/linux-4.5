/*
 *  Driver for Compas P2S0927A2 LCD Controller
 *
 * (C) Copyright 2016 MDB "Compas"
 * Author: Andrew Gazizov, MDB "Compas", <gazizovandrey@live.ru>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/fb.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/gfp.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <video/of_display_timing.h>
#include <video/videomode.h>

#include <video/compas_lcdc.h>

 /* LCD Controller info data structure, stored in device platform_data */
struct compas_lcdfb_info {
	spinlock_t		lock;
	struct fb_info		*info;
	void __iomem		*mmio;

	unsigned int		smem_len;
	struct platform_device	*pdev;
	struct clk		*lcdc_clk;

	u32			pseudo_palette[16];
	bool			have_intensity_bit;

	struct compas_lcdfb_pdata pdata;
};

#define lcdc_readl(sinfo, reg)		__raw_readl((sinfo)->mmio+(reg))
#define lcdc_writel(sinfo, reg, val)	__raw_writel((val), (sinfo)->mmio+(reg))

static const struct platform_device_id compas_lcdfb_devtypes[] = {
	{
		.name = "p2s0927a2-lcdfb",
	}, {
		/* terminator */
	}
};
MODULE_DEVICE_TABLE(platform, compas_lcdfb_devtypes);

#define	COMPAS_LCDFB_FBINFO_DEFAULT	(FBINFO_DEFAULT)

static struct fb_fix_screeninfo compas_lcdfb_fix __initdata = {
	.type		= FB_TYPE_PACKED_PIXELS,
	.visual		= FB_VISUAL_TRUECOLOR,
	.xpanstep	= 0,
	.ypanstep	= 1,
	.ywrapstep	= 0,
	.accel		= FB_ACCEL_NONE,
};

static inline void
compas_lcdfb_free_video_memory(struct compas_lcdfb_info *sinfo)
{
	struct fb_info *info = sinfo->info;

	dma_free_writecombine(info->device, info->fix.smem_len,
				info->screen_base, info->fix.smem_start);
}

/**
 *	compas_lcdfb_alloc_video_memory - Allocate framebuffer memory
 *	@sinfo: the frame buffer to allocate memory for
 * 	
 * 	This function is called only from the compas_lcdfb_probe()
 * 	so no locking by fb_info->mm_lock around smem_len setting is needed.
 */
static int compas_lcdfb_alloc_video_memory(struct compas_lcdfb_info *sinfo)
{
	struct fb_info *info = sinfo->info;
	struct fb_var_screeninfo *var = &info->var;
	unsigned int smem_len;

	smem_len = (var->xres_virtual * var->yres_virtual
		    * ((var->bits_per_pixel + 7) / 8));
	info->fix.smem_len = max(smem_len, sinfo->smem_len);

	info->screen_base = dma_alloc_writecombine(info->device,
			info->fix.smem_len,	(dma_addr_t *)&info->fix.smem_start,
			GFP_KERNEL);

	if (!info->screen_base)
		return -ENOMEM;

	memset(info->screen_base, 0, info->fix.smem_len);

	return 0;
}

static const struct fb_videomode
*compas_lcdfb_choose_mode(struct fb_var_screeninfo *var, struct fb_info *info)
{
	struct fb_videomode varfbmode;
	const struct fb_videomode *fbmode = NULL;

	fb_var_to_videomode(&varfbmode, var);
	fbmode = fb_find_nearest_mode(&varfbmode, &info->modelist);
	if (fbmode)
		fb_videomode_to_var(var, fbmode);
	return fbmode;
}


/**
 *      compas_lcdfb_check_var - Validates a var passed in.
 *      @var: frame buffer variable screen structure
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Checks to see if the hardware supports the state requested by
 *	var passed in. This function does not alter the hardware
 *	state!!!  This means the data stored in struct fb_info and
 *	struct compas_lcdfb_info do not change. This includes the var
 *	inside of struct fb_info.  Do NOT change these. This function
 *	can be called on its own if we intent to only test a mode and
 *	not actually set it. The stuff in modedb.c is a example of
 *	this. If the var passed in is slightly off by what the
 *	hardware can support then we alter the var PASSED in to what
 *	we can do. If the hardware doesn't support mode change a
 *	-EINVAL will be returned by the upper layers. You don't need
 *	to implement this function then. If you hardware doesn't
 *	support changing the resolution then this function is not
 *	needed. In this case the driver would just provide a var that
 *	represents the static state the screen is in.
 *
 *	Returns negative errno on error, or zero on success.
 */
static int compas_lcdfb_check_var(struct fb_var_screeninfo *var,
			     struct fb_info *info)
{
	struct device *dev = info->device;
	struct compas_lcdfb_info *sinfo = info->par;
	unsigned long clk_value_khz;

	clk_value_khz = clk_get_rate(sinfo->lcdc_clk) / 1000;

	dev_dbg(dev, "%s:\n", __func__);

	if (!(var->pixclock && var->bits_per_pixel)) {
		/* choose a suitable mode if possible */
		if (!compas_lcdfb_choose_mode(var, info)) {
			dev_err(dev, "needed value not specified\n");
			return -EINVAL;
		}
	}

	dev_dbg(dev, "  resolution: %ux%u\n", var->xres, var->yres);
	dev_dbg(dev, "  pixclk:     %lu KHz\n", PICOS2KHZ(var->pixclock));
	dev_dbg(dev, "  bpp:        %u\n", var->bits_per_pixel);
	dev_dbg(dev, "  clk:        %lu KHz\n", clk_value_khz);

	if (PICOS2KHZ(var->pixclock) > clk_value_khz) {
		dev_err(dev, "%lu KHz pixel clock is too fast\n", PICOS2KHZ(var->pixclock));
		return -EINVAL;
	}

	/* Do not allow to have real resoulution larger than virtual */
	if (var->xres > var->xres_virtual)
		var->xres_virtual = var->xres;

	if (var->yres > var->yres_virtual)
		var->yres_virtual = var->yres;

	/* Force same alignment for each line */
	var->xres = (var->xres + 3) & ~3UL;
	var->xres_virtual = (var->xres_virtual + 3) & ~3UL;

	var->red.msb_right = var->green.msb_right = var->blue.msb_right = 0;
	var->transp.msb_right = 0;
	var->transp.offset = var->transp.length = 0;
	var->xoffset = var->yoffset = 0;

	if (info->fix.smem_len) {
		unsigned int smem_len = (var->xres_virtual * var->yres_virtual
					 * ((var->bits_per_pixel + 7) / 8));
		if (smem_len > info->fix.smem_len) {
			dev_err(dev, "Frame buffer is too small (%u) for screen size "
					"(need at least %u)\n", info->fix.smem_len, smem_len);
			return -EINVAL;
		}
	}

	/* Saturate vertical and horizontal timings at maximum values */
	var->vsync_len = min_t(u32, var->vsync_len, 0x3FF);
	var->upper_margin = min_t(u32, var->upper_margin, 0xFF);
	var->lower_margin = min_t(u32, var->lower_margin, 0xFF);
	var->right_margin = min_t(u32, var->right_margin, 0xFF);
	var->hsync_len = min_t(u32, var->hsync_len, 0x3FF);
	var->left_margin = min_t(u32, var->left_margin, 0xFF);

	/* Some parameters can't be zero */
	var->vsync_len = max_t(u32, var->vsync_len, 1);
	var->right_margin = max_t(u32, var->right_margin, 1);
	var->hsync_len = max_t(u32, var->hsync_len, 1);
	var->left_margin = max_t(u32, var->left_margin, 1);

	switch (var->bits_per_pixel) {
	case 1:
	case 2:
	case 4:
	case 8:
		var->red.offset = var->green.offset = var->blue.offset = 0;
		var->red.length = var->green.length = var->blue.length
			= var->bits_per_pixel;
		break;
	case 16:
		var->red.offset    = 11;
		var->red.length    = 5;
		var->green.offset  = 5;
		var->green.length  = 6;
		var->blue.offset   = 0;
		var->blue.length   = 5;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 32:
		var->red.offset    = 16;
		var->red.length    = 8;
		var->green.offset  = 8;
		var->green.length  = 8;
		var->blue.offset   = 0;
		var->blue.length   = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	default:
		dev_err(dev, "color depth %d not supported\n",
					var->bits_per_pixel);
		return -EINVAL;
	}

	return 0;
}

/**
 *      compas_lcdfb_set_par - Alters the hardware state.
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Using the fb_var_screeninfo in fb_info we set the resolution
 *	of the this particular framebuffer. This function alters the
 *	par AND the fb_fix_screeninfo stored in fb_info. It doesn't
 *	not alter var in fb_info since we are using that data. This
 *	means we depend on the data in var inside fb_info to be
 *	supported by the hardware.  compas_lcdfb_check_var is always called
 *	before compas_lcdfb_set_par to ensure this.  Again if you can't
 *	change the resolution you don't need this function.
 *
 */
static int compas_lcdfb_set_par(struct fb_info *info)
{
	struct compas_lcdfb_info *sinfo = info->par;
	unsigned long value;
	unsigned long clk_value_khz;
	unsigned long bits_per_line;
	u32 regval;

	dev_dbg(info->device, "%s:\n", __func__);
	dev_dbg(info->device, "  * resolution: %ux%u (%ux%u virtual)\n",
		 info->var.xres, info->var.yres,
		 info->var.xres_virtual, info->var.yres_virtual);

	if (info->var.bits_per_pixel == 1)
		info->fix.visual = FB_VISUAL_MONO01;
	else if (info->var.bits_per_pixel <= 8)
		info->fix.visual = FB_VISUAL_PSEUDOCOLOR;
	else
		info->fix.visual = FB_VISUAL_TRUECOLOR;

	bits_per_line = info->var.xres_virtual * info->var.bits_per_pixel;
	info->fix.line_length = DIV_ROUND_UP(bits_per_line, 8);

	/* Horizontal timing */
	regval = COMPAS_LCDC_ACTIVE(info->var.xres) |
			COMPAS_LCDC_RESET_SYNC(info->var.hsync_len) |
			COMPAS_LCDC_BACK_PORCH(info->var.right_margin) |
			COMPAS_LCDC_FRONT_PORCH(info->var.left_margin);
	dev_dbg(info->device, "  * Horizontal Timing Register = %08lx\n", regval);
	lcdc_writel(sinfo, COMPAS_LCDC_H_TIMING, regval);

	/* Vertical timing */
	regval = COMPAS_LCDC_ACTIVE(info->var.yres) |
			COMPAS_LCDC_RESET_SYNC(info->var.vsync_len) |
			COMPAS_LCDC_BACK_PORCH(info->var.lower_margin) |
			COMPAS_LCDC_FRONT_PORCH(info->var.upper_margin);
	dev_dbg(info->device, "  * Vertical Timing Register = %08lx\n", regval);
	lcdc_writel(sinfo, COMPAS_LCDC_V_TIMING, regval);

	/* Set pixel clock */
	clk_value_khz = clk_get_rate(sinfo->lcdc_clk) / 1000;

	value = DIV_ROUND_UP(clk_value_khz, PICOS2KHZ(info->var.pixclock));

	regval = COMPAS_LCDC_PCLOCK(value);

	info->var.pixclock = KHZ2PICOS(clk_value_khz / value);
	dev_dbg(info->device, "  updated pixclk:     %lu KHz\n",
					PICOS2KHZ(info->var.pixclock));

	if (!(info->var.sync & FB_SYNC_HOR_HIGH_ACT))
		regval |= COMPAS_LCDC_HSYNC_INV;
	if (!(info->var.sync & FB_SYNC_VERT_HIGH_ACT))
		regval |= COMPAS_LCDC_VSYNC_INV;

	switch (info->var.bits_per_pixel) {
		case 16: break;
		case 24: value |= COMPAS_LCDC_24BPP; break;
		default: BUG(); break;
	}

	regval |= COMPAS_LCDC_COLOUR(COMPAS_LCDC_COLOUR_GREEN);

	regval |= COMPAS_LCDC_ENABLE;
	lcdc_writel(sinfo, COMPAS_LCDC_CTRL, regval);

	/* Set framebuffer page address */
	lcdc_writel(sinfo, COMPAS_LCDC_PAGE0_ADDR, info->fix.smem_start);

	dev_dbg(info->device, "  * DONE\n");

	return 0;
}

static struct fb_ops compas_lcdfb_ops = {
	.owner		= THIS_MODULE,
	.fb_check_var	= compas_lcdfb_check_var,
	.fb_set_par		= compas_lcdfb_set_par,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
};

static int __init compas_lcdfb_init_fbinfo(struct compas_lcdfb_info *sinfo)
{
	struct fb_info *info = sinfo->info;
	int ret = 0;

	info->var.activate |= FB_ACTIVATE_FORCE | FB_ACTIVATE_NOW;

	dev_info(info->device,
	       "%luKiB frame buffer at %08lx (mapped at %p)\n",
	       (unsigned long)info->fix.smem_len / 1024,
	       (unsigned long)info->fix.smem_start,
	       info->screen_base);

	/* Allocate colormap */
	ret = fb_alloc_cmap(&info->cmap, 256, 0);
	if (ret < 0)
		dev_err(info->device, "Alloc color map failed\n");

	return ret;
}

static const struct of_device_id compas_lcdfb_dt_ids[] = {
	{ .compatible = "compas,p2s0927a2-lcdc"},
	{ /* sentinel */ }
};

MODULE_DEVICE_TABLE(of, compas_lcdfb_dt_ids);


static int compas_lcdfb_of_init(struct compas_lcdfb_info *sinfo)
{
	struct fb_info *info = sinfo->info;
	struct fb_var_screeninfo *var = &info->var;
	struct device *dev = &sinfo->pdev->dev;
	struct device_node *np =dev->of_node;
	struct device_node *display_np;
	struct device_node *timings_np;
	struct display_timings *timings;
	int ret = -ENOENT;
	int i;

	display_np = of_parse_phandle(np, "display", 0);
	if (!display_np) {
		dev_err(dev, "failed to find display phandle\n");
		return -ENOENT;
	}

	ret = of_property_read_u32(display_np, "bits-per-pixel", &var->bits_per_pixel);
	if (ret < 0) {
		dev_err(dev, "failed to get property bits-per-pixel\n");
		goto put_display_node;
	}

	timings = of_get_display_timings(display_np);
	if (!timings) {
		dev_err(dev, "failed to get display timings\n");
		ret = -EINVAL;
		goto put_display_node;
	}

	timings_np = of_find_node_by_name(display_np, "display-timings");
	if (!timings_np) {
		dev_err(dev, "failed to find display-timings node\n");
		ret = -ENODEV;
		goto put_display_node;
	}

	for (i = 0; i < of_get_child_count(timings_np); i++) {
		struct videomode vm;
		struct fb_videomode fb_vm;

		ret = videomode_from_timings(timings, &vm, i);
		if (ret < 0)
			goto put_timings_node;
		ret = fb_videomode_from_videomode(&vm, &fb_vm);
		if (ret < 0)
			goto put_timings_node;

		fb_add_videomode(&fb_vm, &info->modelist);
	}

	return 0;

put_timings_node:
	of_node_put(timings_np);
put_display_node:
	of_node_put(display_np);
	return ret;
}

static int __init compas_lcdfb_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct fb_info *info;
	struct compas_lcdfb_info *sinfo;
	struct resource *regs = NULL;
	struct fb_modelist *modelist;
	int ret;

	dev_dbg(dev, "%s BEGIN\n", __func__);

	ret = -ENOMEM;
	info = framebuffer_alloc(sizeof(struct compas_lcdfb_info), dev);
	if (!info) {
		dev_err(dev, "cannot allocate memory\n");
		goto out;
	}

	sinfo = info->par;
	sinfo->pdev = pdev;
	sinfo->info = info;

	INIT_LIST_HEAD(&info->modelist);

	ret = compas_lcdfb_of_init(sinfo);
	if (ret) {
		dev_err(dev, "cannot get default configuration\n");
		goto free_info;
	}

	info->flags = COMPAS_LCDFB_FBINFO_DEFAULT;
	info->pseudo_palette = sinfo->pseudo_palette;
	info->fbops = &compas_lcdfb_ops;

	info->fix = compas_lcdfb_fix;
	strcpy(info->fix.id, sinfo->pdev->name);

	/* Enable LCDC Clock */
	sinfo->lcdc_clk = clk_get(dev, "lcdc_clk");
	if (IS_ERR(sinfo->lcdc_clk)) {
		ret = PTR_ERR(sinfo->lcdc_clk);
		goto free_info;
	}
	clk_prepare_enable(sinfo->lcdc_clk);

	modelist = list_first_entry(&info->modelist,
			struct fb_modelist, list);
	fb_videomode_to_var(&info->var, &modelist->mode);

	compas_lcdfb_check_var(&info->var, info);

	regs = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!regs) {
		dev_err(dev, "resources unusable\n");
		ret = -ENXIO;
		goto stop_clk;
	}

	/* Initialize video memory */
	/* allocate memory buffer */
	ret = compas_lcdfb_alloc_video_memory(sinfo);
	if (ret < 0) {
		dev_err(dev, "cannot allocate framebuffer: %d\n", ret);
		goto stop_clk;
	}

	/* LCDC registers */
	info->fix.mmio_start = regs->start;
	info->fix.mmio_len = resource_size(regs);

	if (!request_mem_region(info->fix.mmio_start,
				info->fix.mmio_len, pdev->name)) {
		ret = -EBUSY;
		goto free_fb;
	}

	sinfo->mmio = ioremap(info->fix.mmio_start, info->fix.mmio_len);
	if (!sinfo->mmio) {
		dev_err(dev, "cannot map LCDC registers\n");
		ret = -ENOMEM;
		goto release_mem;
	}

	ret = compas_lcdfb_init_fbinfo(sinfo);
	if (ret < 0) {
		dev_err(dev, "init fbinfo failed: %d\n", ret);
		goto unmap_mmio;
	}

	ret = compas_lcdfb_set_par(info);
	if (ret < 0) {
		dev_err(dev, "set par failed: %d\n", ret);
		goto unmap_mmio;
	}

	dev_set_drvdata(dev, info);

	/*
	 * Tell the world that we're ready to go
	 */
	ret = register_framebuffer(info);
	if (ret < 0) {
		dev_err(dev, "failed to register framebuffer device: %d\n", ret);
		goto reset_drvdata;
	}

	dev_info(dev, "fb%d: Compas LCDC at 0x%08lx (mapped at %p)\n",
			info->node, info->fix.mmio_start, sinfo->mmio);

	return 0;

reset_drvdata:
	dev_set_drvdata(dev, NULL);
unmap_mmio:
	iounmap(sinfo->mmio);
release_mem:
 	release_mem_region(info->fix.mmio_start, info->fix.mmio_len);
free_fb:
	compas_lcdfb_free_video_memory(sinfo);
stop_clk:
	clk_disable_unprepare(sinfo->lcdc_clk);
	clk_put(sinfo->lcdc_clk);
free_info:
	framebuffer_release(info);
out:
	dev_dbg(dev, "%s FAILED\n", __func__);
	return ret;
}

static int __exit compas_lcdfb_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct fb_info *info = dev_get_drvdata(dev);
	struct compas_lcdfb_info *sinfo;
	struct compas_lcdfb_pdata *pdata;

	if (!info || !info->par)
		return 0;
	sinfo = info->par;
	pdata = &sinfo->pdata;

	unregister_framebuffer(info);
	clk_disable_unprepare(sinfo->lcdc_clk);
	clk_put(sinfo->lcdc_clk);
	iounmap(sinfo->mmio);
 	release_mem_region(info->fix.mmio_start, info->fix.mmio_len);
	compas_lcdfb_free_video_memory(sinfo);

	framebuffer_release(info);

	return 0;
}

static struct platform_driver compas_lcdfb_driver = {
	.remove		= __exit_p(compas_lcdfb_remove),
	.id_table	= compas_lcdfb_devtypes,
	.driver		= {
		.name	= "compas_lcdfb",
		.of_match_table	= of_match_ptr(compas_lcdfb_dt_ids),
	},
};

module_platform_driver_probe(compas_lcdfb_driver, compas_lcdfb_probe);

MODULE_DESCRIPTION("Compas LCD Controller framebuffer driver");
MODULE_AUTHOR("Andrew Gazizov <gazizovandrey@live.ru>");
MODULE_LICENSE("GPL");
