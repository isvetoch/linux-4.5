/*
 *  Copyright (C) 2016 MDB Compas
 *	Andrew Gazizov, MDB Compas, <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/clk/p2s_mscu.h>
#include <linux/of.h>

#include "mscu.h"

#define CPU_SOURCE_MAX	2
#define CPU_DIV_MASK	GENMASK(2,0)

struct clk_cpu_characteristics {
	struct clk_range output;
	u32 divisors[5];
};

#define to_clk_cpu(hw) container_of(hw, struct clk_cpu, hw)

struct clk_cpu {
	struct clk_hw hw;
	const struct clk_cpu_characteristics *characteristics;
};

static unsigned long clk_cpu_recalc_rate(struct clk_hw *hw,
					    unsigned long parent_rate)
{
	u8 div;
	unsigned long rate = parent_rate;
	struct clk_cpu *cpu = to_clk_cpu(hw);
	const struct clk_cpu_characteristics *characteristics =
			cpu->characteristics;

	div = p2s_mscu_read(P2S_MSCU_CLKDIV) & CPU_DIV_MASK;

	rate /= characteristics->divisors[div];

	if (rate < characteristics->output.min)
		pr_warn("cpu clk is underclocked");
	else if (rate > characteristics->output.max)
		pr_warn("cpu clk is overclocked");

	return rate;
}

static u8 clk_cpu_get_parent(struct clk_hw *hw)
{
	return p2s_mscu_read(P2S_MSCU_PWMCON) & P2S_MSCU_NORMAL_MODE;
}

static const struct clk_ops cpu_ops = {
	.recalc_rate = clk_cpu_recalc_rate,
	.get_parent = clk_cpu_get_parent,
};

static struct clk * __init p2s_clk_register_cpu(const char *name,
		int num_parents, const char **parent_names,
		const struct clk_cpu_characteristics *characteristics)
{
	struct clk_cpu *cpu;
	struct clk *clk = NULL;
	struct clk_init_data init;

	if (!name || !num_parents || !parent_names)
		return ERR_PTR(-EINVAL);

	cpu = kzalloc(sizeof(*cpu), GFP_KERNEL);
	if (!cpu)
		return ERR_PTR(-ENOMEM);

	init.name = name;
	init.ops = &cpu_ops;
	init.parent_names = parent_names;
	init.num_parents = num_parents;
	init.flags = 0;

	cpu->hw.init = &init;
	cpu->characteristics = characteristics;

	clk = clk_register(NULL, &cpu->hw);
	if (IS_ERR(clk))
		kfree(cpu);

	return clk;
}

static struct clk_cpu_characteristics * __init
of_p2s_clk_cpu_get_characteristics(struct device_node *np)
{
	struct clk_cpu_characteristics *characteristics;

	characteristics = kzalloc(sizeof(*characteristics), GFP_KERNEL);
	if (!characteristics)
		return NULL;

	if (of_p2s_get_clk_range(np, "compas,clk-output-range", &characteristics->output))
		goto out_free_characteristics;

	of_property_read_u32_array(np, "compas,clk-divisors",
				   characteristics->divisors, 5);

	return characteristics;

out_free_characteristics:
	kfree(characteristics);
	return NULL;
}

void __init of_p2s0927a2_clk_cpu_setup(struct device_node *np)
{
	struct clk *clk;
	int num_parents;
	const char *parent_names[CPU_SOURCE_MAX];
	const char *name = np->name;
	struct clk_cpu_characteristics *characteristics;

	num_parents = of_clk_get_parent_count(np);
	if (num_parents <= 0 || num_parents > CPU_SOURCE_MAX)
		return;

	of_clk_parent_fill(np, parent_names, num_parents);

	of_property_read_string(np, "clock-output-names", &name);

	characteristics = of_p2s_clk_cpu_get_characteristics(np);
	if (!characteristics)
		return;

	clk = p2s_clk_register_cpu(name, num_parents, parent_names,
			characteristics);
	if (IS_ERR(clk))
		goto out_free_characteristics;

	of_clk_add_provider(np, of_clk_src_simple_get, clk);
	return;

out_free_characteristics:
	kfree(characteristics);
}
