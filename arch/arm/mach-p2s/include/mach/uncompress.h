#include <mach/p2s.h>

#define AMBA_UART_THR	(*(volatile unsigned char *)P2S_APB0_UART0)
#define AMBA_UART_LSR	(*(volatile unsigned char *)(P2S_APB0_UART0 + 0x14))

/*
 * This does not append a newline
 */
static inline void putc(int c)
{
	while (!(AMBA_UART_LSR & (1 << 5)))
		barrier();

	AMBA_UART_THR = c;
}

static inline void flush(void)
{
	while (AMBA_UART_LSR & (1 << 3))
		barrier();
}

/*
 * nothing to do
 */
#define arch_decomp_setup()

#define arch_decomp_wdog()
