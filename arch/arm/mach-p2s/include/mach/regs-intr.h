#include <mach/p2s.h>

#define INTC0_PHY_ADDR		 P2S_AHB0_INTC

#define INTC0_VIR_ARRD		 IO_ADDRESS_PTR(INTC0_PHY_ADDR)
#define INTC_VA_BASE		 IO_ADDRESS_PTR(INTC0_PHY_ADDR)

#define INTC0_SCR(x) 		(INTC0_VIR_ARRD + 4 * (x))
#define INTC0_ISR    			(INTC0_VIR_ARRD + 0x104)
#define INTC0_IPR    			(INTC0_VIR_ARRD + 0x108)
#define INTC0_IMR    			(INTC0_VIR_ARRD + 0x10C)
#define INTC0_IECR   			(INTC0_VIR_ARRD + 0x114)
#define INTC0_ICCR   			(INTC0_VIR_ARRD + 0x118)
#define INTC0_ISCR   			(INTC0_VIR_ARRD + 0x11C)
#define INTC0_TEST   			(INTC0_VIR_ARRD + 0x124)
