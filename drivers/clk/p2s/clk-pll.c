/*
 *  Copyright (C) 2016 MDB Compas
 *	Andrew Gazizov, MDB Compas, <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/clk/p2s_mscu.h>
#include <linux/of.h>

#include "mscu.h"

#define PLL_STATUS_MASK		GENMASK(23, 22)
#define PLL_CON(id)			(P2S_MSCU_PLL1CON + ((id) * 4))
#define PLL_DIVR_MASK		0x1F
#define PLL_DIVR(reg)		((reg >> 10) & PLL_DIVR_MASK)
#define PLL_DIVQ_MASK		0x07
#define PLL_DIVQ(reg)		((reg >> 7) & PLL_DIVQ_MASK)
#define PLL_DIVF_MASK		0x7F
#define PLL_DIVF(reg)		((reg) & PLL_DIVF_MASK)

#define PLL_MAX_ID	1

struct clk_pll_characteristics {
	struct clk_range input;
	struct clk_range output;
};

#define to_clk_pll(hw) container_of(hw, struct clk_pll, hw)

struct clk_pll {
	struct clk_hw hw;
	u8 id;
	u8 divr;
	u8 divf;
	u8 divq;
	u8 range;
	const struct clk_pll_characteristics *characteristics;
};

static int clk_pll_prepare(struct clk_hw *hw)
{
	printk("%s\n", __func__);
	return 0;
}

static void clk_pll_unprepare(struct clk_hw *hw)
{
	printk("%s\n", __func__);
}

static int clk_pll_is_prepared(struct clk_hw *hw)
{
	struct clk_pll *pll = to_clk_pll(hw);

	printk("%s\n", __func__);
	return !!(p2s_mscu_read(PLL_CON(pll->id)) & PLL_STATUS_MASK);
}

static unsigned long clk_pll_recalc_rate(struct clk_hw *hw,
					 unsigned long parent_rate)
{
	struct clk_pll *pll = to_clk_pll(hw);

	if (!pll->divr || !pll->divf || !pll->divq)
			return 0;

	return parent_rate / (pll->divr + 1) * (pll->divf + 1) / (1 << pll->divq);
}

static int clk_pll_set_rate(struct clk_hw *hw, unsigned long rate,
			    unsigned long parent_rate)
{
	printk("%s\n", __func__);
	return 0;
}

static long clk_pll_round_rate(struct clk_hw *hw, unsigned long rate,
					unsigned long *parent_rate)
{
	printk("%s\n", __func__);
	return 0;
}

static const struct clk_ops pll_ops = {
	.prepare = clk_pll_prepare,
	.unprepare = clk_pll_unprepare,
	.is_prepared = clk_pll_is_prepared,
	.recalc_rate = clk_pll_recalc_rate,
	.round_rate = clk_pll_round_rate,
	.set_rate = clk_pll_set_rate,
};

static struct clk * __init p2s_clk_register_pll(const char *name,
		const char *parent_name, u8 id,
		const struct clk_pll_characteristics *characteristics)
{
	struct clk_pll *pll;
	struct clk *clk = NULL;
	struct clk_init_data init;
	int offset = PLL_CON(id);
	u32 tmp;

	if (id > PLL_MAX_ID)
		return ERR_PTR(-EINVAL);

	pll = kzalloc(sizeof(*pll), GFP_KERNEL);
	if (!pll)
		return ERR_PTR(-ENOMEM);

	init.name = name;
	init.ops = &pll_ops;
	init.parent_names = &parent_name;
	init.num_parents = 1;
	init.flags = CLK_SET_RATE_GATE;

	pll->id = id;
	pll->hw.init = &init;
	pll->characteristics = characteristics;

	tmp = p2s_mscu_read(offset);
	pll->divr = PLL_DIVR(tmp);
	pll->divq = PLL_DIVQ(tmp);
	pll->divf = PLL_DIVF(tmp);

	printk("divr %d divq %d divf %d\n", pll->divr, pll->divq, pll->divf);

	clk = clk_register(NULL, &pll->hw);
	if (IS_ERR(clk))
		kfree(pll);

	return clk;
}

static struct clk_pll_characteristics * __init
of_p2s_clk_pll_get_characteristics(struct device_node *np)
{
	struct clk_pll_characteristics *characteristics;
	struct clk_range input;
	struct clk_range output;

	if (of_p2s_get_clk_range(np, "compas,clk-input-range", &input))
		return NULL;

	if (of_p2s_get_clk_range(np, "compas,pll-clk-output-ranges", &output))
			return NULL;

	characteristics = kzalloc(sizeof(*characteristics), GFP_KERNEL);
	if (!characteristics)
		return NULL;

	characteristics->input = input;
	characteristics->output = output;
	return characteristics;
}

void __init of_p2s0927a2_clk_pll_setup(struct device_node *np)
{
	u32 id;
	struct clk *clk;
	const char *parent_name;
	const char *name = np->name;
	struct clk_pll_characteristics *characteristics;

	if (of_property_read_u32(np, "reg", &id))
		return;

	parent_name = of_clk_get_parent_name(np, 0);

	of_property_read_string(np, "clock-output-names", &name);
	printk("PLL name: %s: ", name);

	characteristics = of_p2s_clk_pll_get_characteristics(np);
	if (!characteristics)
		return;

	clk = p2s_clk_register_pll(name, parent_name, id, characteristics);
	if (IS_ERR(clk))
		goto out_free_characteristics;

	of_clk_add_provider(np, of_clk_src_simple_get, clk);
	return;

out_free_characteristics:
	kfree(characteristics);
}
