/*
 * Compas SD/MMC/SDIO Host Controller driver.
 *
 * (C) Copyright 2016 MDB "Compas"
 * Author: Andrew Gazizov, MDB "Compas", <gazizovandrey@live.ru>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/mmc/host.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>

#include "sdhci-pltfm.h"

static const struct sdhci_ops sdhci_p2s0297a2_ops = {
	.set_clock			= sdhci_set_clock,
	.get_max_clock		= sdhci_pltfm_clk_get_max_clock,
	.set_bus_width		= sdhci_set_bus_width,
	.reset				= sdhci_reset,
	.set_uhs_signaling	= sdhci_set_uhs_signaling,
};

static const struct sdhci_pltfm_data soc_data_p2s0927a2 = {
	.ops = &sdhci_p2s0297a2_ops,
	.quirks = SDHCI_QUIRK_INVERTED_WRITE_PROTECT |
	SDHCI_QUIRK_DATA_TIMEOUT_USES_SDCLK | SDHCI_QUIRK_BROKEN_ADMA |
	SDHCI_QUIRK_CAP_CLOCK_BASE_BROKEN,
};

static const struct of_device_id sdhci_p2s_dt_match[] = {
	{ .compatible = "compas,p2s0927a2-sdhci", .data = &soc_data_p2s0927a2 },
	{}
};

static int sdhci_p2s_probe(struct platform_device *pdev)
{
	const struct of_device_id		*match;
	const struct sdhci_pltfm_data	*soc_data;
	struct sdhci_pltfm_host			*pltfm_host;
	struct sdhci_host				*host;
	int								ret;

	match = of_match_device(sdhci_p2s_dt_match, &pdev->dev);
	if (!match)
		return -EINVAL;
	soc_data = match->data;

	host = sdhci_pltfm_init(pdev, soc_data, 0);
	if (IS_ERR(host))
		return PTR_ERR(host);

	pltfm_host = sdhci_priv(host);

	pltfm_host->clk = devm_clk_get(&pdev->dev, "sdio_clk");
	if (IS_ERR(pltfm_host->clk)) {
		dev_err(&pdev->dev, "failed to get sdio clk\n");
		return PTR_ERR(pltfm_host->clk);
	}

	clk_prepare_enable(pltfm_host->clk);

	ret = mmc_of_parse(host->mmc);
	if (ret)
		goto clocks_disable_unprepare;

	/* go to slot0, slot1 now is not supported */
	host->ioaddr += 0x100;

	sdhci_get_of_property(pdev);

	ret = sdhci_add_host(host);
	if (ret)
		goto clocks_disable_unprepare;

	return 0;

clocks_disable_unprepare:
	clk_disable_unprepare(pltfm_host->clk);
	sdhci_pltfm_free(pdev);
	return ret;
}

static int sdhci_p2s_remove(struct platform_device *pdev)
{
	struct sdhci_host	*host = platform_get_drvdata(pdev);
	struct sdhci_pltfm_host	*pltfm_host = sdhci_priv(host);

	sdhci_pltfm_unregister(pdev);

	clk_disable_unprepare(pltfm_host->priv);
	return 0;
}

static struct platform_driver sdhci_p2s_driver = {
	.driver		= {
		.name	= "sdhci-p2s",
		.of_match_table = sdhci_p2s_dt_match,
	},
	.probe		= sdhci_p2s_probe,
	.remove		= sdhci_p2s_remove,
};

module_platform_driver(sdhci_p2s_driver);

MODULE_DESCRIPTION("SDHCI driver for p2s");
MODULE_AUTHOR("Andrew Gazizov <gazizovandrey@live.ru>");
MODULE_LICENSE("GPL v2");
