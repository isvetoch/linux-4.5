/*
 *  Copyright (C) 2016 MDB Compas
 *	Andrew Gazizov, MDB Compas, <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/clk/p2s_mscu.h>
#include <linux/of.h>
#include <linux/types.h>

#include "mscu.h"

#define PERIPHERAL_MAX		32

#define PERIPHERAL_ID_MIN	2
#define PERIPHERAL_ID_MAX	31
#define PERIPHERAL_MASK(id)	(1 << ((id) & PERIPHERAL_ID_MAX))

struct clk_peripheral {
	struct clk_hw hw;
	u32 id;
	u8 div2;
};

#define to_clk_peripheral(hw) container_of(hw, struct clk_peripheral, hw)

static int clk_peripheral_enable(struct clk_hw *hw)
{
	struct clk_peripheral *periph = to_clk_peripheral(hw);
	u32 regval;
	printk("%s\n", __func__);
	regval = p2s_mscu_read(P2S_MSCU_CLKCFG);
	p2s_mscu_write(P2S_MSCU_CLKCFG, regval & ~PERIPHERAL_MASK(periph->id));
	return 0;
}

static void clk_peripheral_disable(struct clk_hw *hw)
{
	struct clk_peripheral *periph = to_clk_peripheral(hw);
	u32 regval;
	printk("%s\n", __func__);
	regval = p2s_mscu_read(P2S_MSCU_CLKCFG);
	p2s_mscu_write(P2S_MSCU_CLKCFG, regval | PERIPHERAL_MASK(periph->id));
}

static int clk_peripheral_is_enabled(struct clk_hw *hw)
{
	struct clk_peripheral *periph = to_clk_peripheral(hw);
	printk("%s\n", __func__);
	return !(p2s_mscu_read(P2S_MSCU_CLKCFG) & PERIPHERAL_MASK(periph->id));
}

static unsigned long clk_peripheral_recalc_rate(struct clk_hw *hw,
					    unsigned long parent_rate)
{
	struct clk_peripheral *periph = to_clk_peripheral(hw);

	if (periph->div2)
		return parent_rate / 2;
	else
		return parent_rate;
}

static const struct clk_ops peripheral_ops = {
	.enable = clk_peripheral_enable,
	.disable = clk_peripheral_disable,
	.is_enabled = clk_peripheral_is_enabled,
	.recalc_rate = clk_peripheral_recalc_rate,
};

static struct clk * __init p2s_clk_register_peripheral(const char *name,
		const char *parent_name, u32 id, bool div2)
{
	struct clk_peripheral *periph;
	struct clk *clk = NULL;
	struct clk_init_data init;

	if (!name || !parent_name || id > PERIPHERAL_ID_MAX)
		return ERR_PTR(-EINVAL);

	periph = kzalloc(sizeof(*periph), GFP_KERNEL);
	if (!periph)
		return ERR_PTR(-ENOMEM);

	init.name = name;
	init.ops = &peripheral_ops;
	init.parent_names = &parent_name;
	init.num_parents = 1;
	init.flags = 0;

	periph->id = id;
	periph->hw.init = &init;
	periph->div2 = div2;

	clk = clk_register(NULL, &periph->hw);
	if (IS_ERR(clk))
		kfree(periph);

	return clk;
}

void __init of_p2s0927a2_clk_periph_setup(struct device_node *np)
{
	int num;
	u32 id;
	bool div2;
	struct clk *clk;
	const char *parent_name;
	const char *name;
	struct device_node *periphclknp;

	parent_name = of_clk_get_parent_name(np, 0);
	if (!parent_name)
		return;

	num = of_get_child_count(np);
	if (!num || num > PERIPHERAL_MAX)
		return;

	for_each_child_of_node(np, periphclknp) {
		if (of_property_read_u32(periphclknp, "reg", &id))
			continue;

		if (id >= PERIPHERAL_MAX)
			continue;

		if (of_property_read_string(np, "clock-output-names", &name))
			name = periphclknp->name;

		div2 = of_property_read_bool(periphclknp, "compas,amba-div2");
		clk = p2s_clk_register_peripheral(name, parent_name, id, div2);

		if (IS_ERR(clk))
			continue;

		of_clk_add_provider(periphclknp, of_clk_src_simple_get, clk);
	}
}
