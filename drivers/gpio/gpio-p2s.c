/*
 * Compas P2S0927A2 GPIO controller driver
 *
 * (C) Copyright 2016 MDB "Compas"
 * Author: Andrew Gazizov, MDB "Compas", <gazizovandrey@live.ru>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/clk.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/gpio/driver.h>
#include <linux/seq_file.h>

#define GPIO_PDR(x)		((x) * 8)			// Port x data register
#define GPIO_PCON(x) 	(((x) * 8) + 4)		// Port x direction register
#define GPIO_IE(x)		(0x24 + ((x) * 4))	// Port x interrupt mask register
#define GPIO_IS(x)		(0x34 + ((x) * 4))	// Port x interrupt sense register
#define GPIO_IBE(x)		(0x44 + ((x) * 4))	// Port x interrupt both-edges reg
#define GPIO_IEV(x)		(0x54 + ((x) * 4))	// Port x interrupt sense register
#define GPIO_IC(x)		(0x64 + ((x) * 4))	// Port x interrupt clear register
#define GPIO_ISR		0x74				// GPIO interrupt status register

#define MAX_GPIO			32
#define MAX_GPIO_PER_PORT	8

#define GPIO_EDGE				0
#define GPIO_LEVEL				1

#define GPIO_SINGLE_EDGE		0
#define GPIO_BOTH_EDGES			1

#define GPIO_FALLING_EDGE		0
#define GPIO_RISING_EDGE		1

#define GPIO_LOW_LEVEL			0
#define GPIO_HIGH_LEVEL			1

struct p2s_gpio_chip {
	struct gpio_chip	chip;
	int			gpioc_hwirq;	/* GPIO bank interrupt identifier on INTC */
	int			gpioc_virq;	/* GPIO bank Linux virtual interrupt */
	void __iomem		*regbase;	/* GPIO bank virtual address */
	struct clk		*clock;		/* associated clock */
};

static inline unsigned char pin_to_port(unsigned pin)
{
	return pin /= MAX_GPIO_PER_PORT;
}

static inline unsigned char pin_to_mask(unsigned int pin)
{
	pin %= MAX_GPIO_PER_PORT;
	return 1 << pin;
}

static int p2s_gpio_get_direction(struct gpio_chip *chip, unsigned offset)
{
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_PCON(pin_to_port(offset)));
	return !(regval & pin_to_mask(offset));
}

static int p2s_gpio_direction_input(struct gpio_chip *chip, unsigned offset)
{
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_PCON(pin_to_port(offset)));
	regval &= ~pin_to_mask(offset);
	writel_relaxed(regval, gpio + GPIO_PCON(pin_to_port(offset)));
	return 0;
}

static int p2s_gpio_direction_output(struct gpio_chip *chip, unsigned offset)
{
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_PCON(pin_to_port(offset)));
	regval |= pin_to_mask(offset);
	writel_relaxed(regval, gpio + GPIO_PCON(pin_to_port(offset)));
	return 0;
}

static int p2s_gpio_get(struct gpio_chip *chip, unsigned offset)
{
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio +  GPIO_PDR(pin_to_port(offset)));
	return (regval & pin_to_mask(offset)) != 0;
}

static void p2s_gpio_set(struct gpio_chip *chip, unsigned offset, int val)
{
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_PDR(pin_to_port(offset)));

	if (val)
		regval |= pin_to_mask(offset);
	else
		regval &= ~pin_to_mask(offset);

	writel_relaxed(regval, gpio + GPIO_PDR(pin_to_port(offset)));
}

#ifdef CONFIG_DEBUG_FS
static void p2s_gpio_dbg_show(struct seq_file *s, struct gpio_chip *chip)
{
	int i;
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(chip);
	void __iomem *gpio = p2s_gpio->regbase;

	for (i = 0; i < chip->ngpio; i++) {
		unsigned mask = pin_to_mask(i);
		const char *gpio_label;

		gpio_label = gpiochip_is_requested(chip, i);
		if (!gpio_label)
			continue;

		seq_printf(s, "[%s] GPIO%s%d: ",
			   gpio_label, chip->label, i);
		seq_printf(s, "[port%c_%d]", i / 8 + 'A', i % 8);
		seq_printf(s, "%s ",
				readl_relaxed(gpio + GPIO_PCON(pin_to_port(i))) & pin_to_mask(i)
				? "output" : "input");
		seq_printf(s, "%s\n",
				readl_relaxed(gpio + GPIO_PDR(pin_to_port(i))) & pin_to_mask(i)
				? "set" : "clear");
	}
}
#else
#define p2s_gpio_dbg_show	NULL
#endif

static const struct of_device_id p2s_gpio_of_match[] = {
	{ .compatible = "compas,p2s0927a2-gpio" },
	{ /* sentinel */ }
};


static void gpio_irq_mask(struct irq_data *d)
{
	struct p2s_gpio_chip *p2s_gpio = irq_data_get_irq_chip_data(d);
	void __iomem	*gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_IE(pin_to_port(d->hwirq)));
	regval &= ~pin_to_mask(d->hwirq);
	writel_relaxed(regval, gpio + GPIO_IE(pin_to_port(d->hwirq)));
}

static void gpio_irq_unmask(struct irq_data *d)
{
	struct p2s_gpio_chip *p2s_gpio = irq_data_get_irq_chip_data(d);
	void __iomem	*gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_IE(pin_to_port(d->hwirq)));
	regval |= pin_to_mask(d->hwirq);
	writel_relaxed(regval, gpio + GPIO_IE(pin_to_port(d->hwirq)));
}

static void
p2s_gpio_set_interrupt_sense(struct irq_data *d, unsigned value)
{
	struct p2s_gpio_chip *p2s_gpio = irq_data_get_irq_chip_data(d);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_IS(pin_to_port(d->hwirq)));

	if (value)
		regval |= pin_to_mask(d->hwirq);
	else
		regval &= ~pin_to_mask(d->hwirq);

	writel_relaxed(regval, gpio + GPIO_IS(pin_to_port(d->hwirq)));
}

static void
p2s_gpio_set_interrupt_both_edges(struct irq_data *d, unsigned value)
{
	struct p2s_gpio_chip *p2s_gpio = irq_data_get_irq_chip_data(d);
	void __iomem *gpio = p2s_gpio->regbase;
	u32 regval;

	regval = readl_relaxed(gpio + GPIO_IBE(pin_to_port(d->hwirq)));

	if (value)
		regval |= pin_to_mask(d->hwirq);
	else
		regval &= ~pin_to_mask(d->hwirq);

	writel_relaxed(regval, gpio + GPIO_IBE(pin_to_port(d->hwirq)));
}


static void
p2s_gpio_set_interrupt_event(struct irq_data *d, unsigned value)
{
	struct p2s_gpio_chip *p2s_gpio = irq_data_get_irq_chip_data(d);
	void __iomem *gpio = p2s_gpio->regbase;

	u32 regval;

	regval = readl_relaxed(gpio + GPIO_IEV(pin_to_port(d->hwirq)));

	if (value)
		regval |= pin_to_mask(d->hwirq);
	else
		regval &= ~pin_to_mask(d->hwirq);

	writel_relaxed(regval, gpio + GPIO_IEV(pin_to_port(d->hwirq)));
}

static int gpio_irq_type(struct irq_data *d, unsigned int type)
{
	switch (type) {
	case IRQ_TYPE_EDGE_RISING:
		irq_set_handler_locked(d, handle_simple_irq);
		p2s_gpio_set_interrupt_sense(d, GPIO_EDGE);
		p2s_gpio_set_interrupt_both_edges(d, GPIO_SINGLE_EDGE);
		p2s_gpio_set_interrupt_event(d, GPIO_RISING_EDGE);
		break;
	case IRQ_TYPE_EDGE_FALLING:
		irq_set_handler_locked(d, handle_simple_irq);
		p2s_gpio_set_interrupt_sense(d, GPIO_EDGE);
		p2s_gpio_set_interrupt_both_edges(d, GPIO_SINGLE_EDGE);
		p2s_gpio_set_interrupt_event(d, GPIO_FALLING_EDGE);
		break;
	case IRQ_TYPE_EDGE_BOTH:
		irq_set_handler_locked(d, handle_simple_irq);
		p2s_gpio_set_interrupt_sense(d, GPIO_EDGE);
		p2s_gpio_set_interrupt_both_edges(d, GPIO_BOTH_EDGES);
		break;

	case IRQ_TYPE_LEVEL_LOW:
		irq_set_handler_locked(d, handle_level_irq);
		p2s_gpio_set_interrupt_sense(d, GPIO_LEVEL);
		p2s_gpio_set_interrupt_event(d, GPIO_LOW_LEVEL);
		break;
	case IRQ_TYPE_LEVEL_HIGH:
		irq_set_handler_locked(d, handle_level_irq);
		p2s_gpio_set_interrupt_sense(d, GPIO_LEVEL);
		p2s_gpio_set_interrupt_event(d, GPIO_LOW_LEVEL);
		break;
		case IRQ_TYPE_NONE:
	default:
		pr_warn("P2S: No type for irq %d\n", gpio_to_irq(d->irq));
		return -EINVAL;
	}

	return 0;
}

static void gpio_irq_ack(struct irq_data *d)
{
	struct gpio_chip *gpio_chip = irq_desc_get_handler_data(d);
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(gpio_chip);
	void __iomem	*gpio = p2s_gpio->regbase;

	writel_relaxed(pin_to_mask(d->hwirq), gpio + GPIO_IC(pin_to_port(d->hwirq)));
}

static int gpio_irq_set_wake(struct irq_data *d, unsigned int state)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);

	return irq_set_irq_wake(gc->irq_parent, state);
}

static struct irq_chip gpio_irqchip = {
	.name		= "GPIO",
	.irq_ack	= gpio_irq_ack,
	.irq_disable	= gpio_irq_mask,
	.irq_mask	= gpio_irq_mask,
	.irq_unmask	= gpio_irq_unmask,
	.irq_set_type = gpio_irq_type,
	.irq_set_wake = gpio_irq_set_wake,
};

static void gpio_irq_handler(struct irq_desc *desc)
{
	struct irq_chip *chip = irq_desc_get_chip(desc);
	struct gpio_chip *gpio_chip = irq_desc_get_handler_data(desc);
	struct p2s_gpio_chip *p2s_gpio = gpiochip_get_data(gpio_chip);
	void __iomem	*gpio = p2s_gpio->regbase;
	unsigned long	isr;
	int		n;
	chained_irq_enter(chip, desc);

	isr = readl_relaxed(gpio + GPIO_ISR);

	for_each_set_bit(n, &isr, BITS_PER_LONG) {
		// Clear interrupt
		writel_relaxed(pin_to_mask(n), gpio + GPIO_IC(pin_to_port(n)));
		// Handle it
		generic_handle_irq(irq_find_mapping(gpio_chip->irqdomain, n));
	}

	chained_irq_exit(chip, desc);
}

static int p2s_gpio_of_irq_setup(struct platform_device *pdev,
				  struct p2s_gpio_chip *p2s_gpio)
{
	struct gpio_chip	*gpiochip_prev = NULL;
	struct p2s_gpio_chip   *prev = NULL;
	struct irq_data		*d = irq_get_irq_data(p2s_gpio->gpioc_virq);
	int ret, i;

	p2s_gpio->gpioc_hwirq = irqd_to_hwirq(d);

	/* Disable irqs on all GPIO controller ports */
	for (i = 0; i < 4; i++)
		writel_relaxed(0, p2s_gpio->regbase + GPIO_IE(i));

	/*
	 * Let the generic code handle this edge IRQ, the the chained
	 * handler will perform the actual work of handling the parent
	 * interrupt.
	 */
	ret = gpiochip_irqchip_add(&p2s_gpio->chip, &gpio_irqchip, 0,
			handle_edge_irq, IRQ_TYPE_EDGE_BOTH);
	if (ret) {
		dev_err(&pdev->dev, "p2s_gpio: Couldn't add irqchip to gpiochip.\n");
		return ret;
	}

	gpiochip_set_chained_irqchip(&p2s_gpio->chip, &gpio_irqchip,
			p2s_gpio->gpioc_virq, gpio_irq_handler);

	return 0;
}

static int p2s_gpio_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct resource *res;
	struct p2s_gpio_chip *p2s_chip = NULL;
	struct gpio_chip *chip;
	int ret = 0;
	int irq, i;
	char **names;

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		ret = irq;
		goto err;
	}

	p2s_chip = devm_kzalloc(&pdev->dev, sizeof(*p2s_chip), GFP_KERNEL);
	if (!p2s_chip) {
		ret = -ENOMEM;
		goto err;
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	p2s_chip->regbase = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(p2s_chip->regbase)) {
		ret = PTR_ERR(p2s_chip->regbase);
		goto err;
	}

	p2s_chip->gpioc_virq = irq;

	p2s_chip->clock = devm_clk_get(&pdev->dev, NULL);
	if (IS_ERR(p2s_chip->clock)) {
		dev_err(&pdev->dev, "failed to get clock, ignoring.\n");
		ret = PTR_ERR(p2s_chip->clock);
		goto err;
	}

	ret = clk_prepare(p2s_chip->clock);
	if (ret)
		goto clk_prepare_err;

	/* enable GPIO controller's clock */
	ret = clk_enable(p2s_chip->clock);
	if (ret) {
		dev_err(&pdev->dev, "failed to enable clock, ignoring.\n");
		goto clk_enable_err;
	}
	chip = &p2s_chip->chip;

	chip->request			= gpiochip_generic_request;
	chip->free				= gpiochip_generic_free;
	chip->get_direction		= p2s_gpio_get_direction;
	chip->direction_input	= p2s_gpio_direction_input,
	chip->get				= p2s_gpio_get,
	chip->direction_output	= p2s_gpio_direction_output,
	chip->set				= p2s_gpio_set,
	chip->dbg_show			= p2s_gpio_dbg_show,
	chip->can_sleep			= false,
	chip->ngpio				= MAX_GPIO,
	chip->of_node 			= np;
	chip->label 			= dev_name(&pdev->dev);
	chip->parent 			= &pdev->dev;
	chip->owner 			= THIS_MODULE;
	chip->base 				= 0;

	names = devm_kzalloc(&pdev->dev, sizeof(char *) * chip->ngpio,
			     GFP_KERNEL);

	if (!names) {
		ret = -ENOMEM;
		goto clk_enable_err;
	}

	for (i = 0; i < chip->ngpio; i++)
		names[i] = kasprintf(GFP_KERNEL, "port%c_%d", i / 8 + 'A', i % 8);

	chip->names = (const char *const *)names;

	ret = gpiochip_add_data(chip, p2s_chip);
	if (ret)
		goto gpiochip_add_err;

	ret = p2s_gpio_of_irq_setup(pdev, p2s_chip);
	if (ret)
		goto irq_setup_err;

	dev_info(&pdev->dev, "at address %p\n", p2s_chip->regbase);

	return 0;

irq_setup_err:
	gpiochip_remove(chip);
gpiochip_add_err:
	clk_disable(p2s_chip->clock);
clk_enable_err:
	clk_unprepare(p2s_chip->clock);
clk_prepare_err:
err:
	dev_err(&pdev->dev, "Failure %i\n", ret);

	return ret;
}

static struct platform_driver p2s_gpio_driver = {
	.driver = {
		.name = "gpio-p2s",
		.of_match_table = p2s_gpio_of_match,
	},
	.probe = p2s_gpio_probe,
};

module_platform_driver(p2s_gpio_driver);
MODULE_DESCRIPTION("Compas GPIO controller driver");
MODULE_AUTHOR("Andrew Gazizov <gazizovandrey@live.ru>");
MODULE_LICENSE("GPL");
