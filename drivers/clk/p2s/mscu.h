/*
 * drivers/clk/p2s/mscu.h
 *
 *  Copyright (C) 2016 MDB Compas
 *
 *	Andrew Gazizov   <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __MSCU_H_
#define __MSCU_H_

struct clk_range {
	unsigned long min;
	unsigned long max;
};

#define CLK_RANGE(MIN, MAX) {.min = MIN, .max = MAX,}

int of_p2s_get_clk_range(struct device_node *np, const char *propname,
			  struct clk_range *range);

void of_p2s0927a2_clk_pll_setup(struct device_node *np);
void of_p2s0927a2_clk_cpu_setup(struct device_node *np);
void of_p2s0927a2_clk_amba_setup(struct device_node *np);
void of_p2s0927a2_clk_periph_setup(struct device_node *np);

#endif /* __MSCU_H_ */
