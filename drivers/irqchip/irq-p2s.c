/*
 * Compas P2S0927A2 Interrupt Controller driver
 *
 *  Copyright (C) 2016 MDB Compas
 *
 *  Author: Andrew Gazizov 	<gazizovandrey@live.ru>
 *						 	<a.gazizov@mkb-kompas.ru>
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>

#include <linux/irq.h>
#include <linux/irqdomain.h>
#include <linux/irqchip.h>
#include <asm/exception.h>

#define P2S_INTC_SRCTYPE			GENMASK(7, 6)
#define P2S_INTC_SRCTYPE_LOW		(0 << 6)
#define P2S_INTC_SRCTYPE_HIGH		(1 << 6)
#define P2S_INTC_SRCTYPE_NEGATIVE	(2 << 6)
#define P2S_INTC_SRCTYPE_POSITIVE	(3 << 6)

/* Number of irq lines managed by INTC */
#define NR_INTC_IRQS	32

#define P2S_IRQ_SCR(n)	((n) * 4)
#define P2S_IRQ_SVR(n)	(0x80 + ((n) * 4))
#define P2S_IRQ_IVR		0x100
#define P2S_IRQ_ISR		0x104
#define P2S_IRQ_IPR		0x108
#define P2S_IRQ_IMR		0x10C
#define P2S_IRQ_IECR	0x114
#define P2S_IRQ_ICCR	0x118
#define P2S_IRQ_ISCR	0x11C
#define P2S_IRQ_TEST	0x124

static struct irq_domain *intc_domain;
static void __iomem *reg_base;

static asmlinkage void __exception_irq_entry intc_handle(struct pt_regs *regs)
{
	struct irq_domain_chip_generic *dgc = intc_domain->gc;
	struct irq_chip_generic *gc = dgc->gc[0];
	u32 irqnr;

	irqnr = irq_reg_readl(gc, P2S_IRQ_ISR);
	handle_domain_irq(intc_domain, irqnr, regs);
}

static int intc_set_type(struct irq_data *data, unsigned int type)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	u32 regval;
	unsigned short intc_type;

	switch (type) {
		case IRQ_TYPE_EDGE_RISING:
			intc_type = P2S_INTC_SRCTYPE_POSITIVE;
			break;
		case IRQ_TYPE_EDGE_FALLING:
			intc_type = P2S_INTC_SRCTYPE_NEGATIVE;
			break;
		case IRQ_TYPE_LEVEL_LOW:
			intc_type = P2S_INTC_SRCTYPE_LOW;
			break;
		case IRQ_TYPE_LEVEL_HIGH:
			intc_type = P2S_INTC_SRCTYPE_HIGH;
			break;
		default:
			return -EINVAL;
		}
	regval = irq_reg_readl(gc, P2S_IRQ_SCR(data->hwirq));
	regval &= ~P2S_INTC_SRCTYPE;
	irq_reg_writel(gc, regval | intc_type, P2S_IRQ_SCR(data->hwirq));
	return 0;
}

static inline void intc_enable(struct irq_data *data)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	u32 regval;
	regval = irq_reg_readl(gc, P2S_IRQ_IECR);
	irq_reg_writel(gc, regval | (1 << data->hwirq), P2S_IRQ_IECR);
}

static inline  void intc_disable(struct irq_data *data)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	u32 regval;
	regval = irq_reg_readl(gc, P2S_IRQ_IECR);
	irq_reg_writel(gc, regval & ~(1 << data->hwirq), P2S_IRQ_IECR);
}

static inline void intc_set_mask(struct irq_data *data)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	u32 regval;
	regval = irq_reg_readl(gc, P2S_IRQ_IMR);
	irq_reg_writel(gc, regval | (1 << data->hwirq), P2S_IRQ_IMR);
}

static inline void intc_clr_mask(struct irq_data *data)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	u32 regval;
	regval = irq_reg_readl(gc, P2S_IRQ_IMR);
	irq_reg_writel(gc, regval & ~(1 << data->hwirq), P2S_IRQ_IMR);
}

static void p2s_mask_irq(struct irq_data *data)
{
	struct irq_chip_generic *gc = irq_data_get_irq_chip_data(data);
	intc_disable(data);
	intc_clr_mask(data);
	irq_reg_writel(gc, (1 << (data->hwirq)), P2S_IRQ_ICCR);
}

static void p2s_unmask_irq(struct irq_data *data)
{
	intc_set_mask(data);
	intc_enable(data);
}

static const struct irq_domain_ops intc_irq_ops = {
	.map	= irq_map_generic_chip,
	.xlate	= irq_domain_xlate_onecell,
};

static void __init intc_hw_init(struct irq_domain *domain)
{
	struct irq_chip_generic *gc = irq_get_domain_generic_chip(domain, 0);

	/* Disable and clear all interrupts initially */
	irq_reg_writel(gc, 0, P2S_IRQ_IECR);
	irq_reg_writel(gc, 0xFFFFFFFF, P2S_IRQ_ICCR);
}

static int __init intc_of_init(struct device_node *node,
			      struct device_node *parent)
{
	struct irq_chip_generic *gc;
	struct irq_domain *domain;
	int ret;

	reg_base = of_iomap(node, 0);
	if (!reg_base)
		return -ENOMEM;

	if (intc_domain)
		return -EEXIST;

	domain = irq_domain_add_linear(node, NR_INTC_IRQS,
			&irq_generic_chip_ops, NULL);
	if (!domain)
		return -ENOMEM;

	ret = irq_alloc_domain_generic_chips(domain, NR_INTC_IRQS, 1,
			"compas-intc", handle_simple_irq,
			IRQ_NOREQUEST | IRQ_NOPROBE | IRQ_NOAUTOEN, 0, 0);
	if (ret)
		goto err_domain_remove;

	intc_domain = domain;
	gc = irq_get_domain_generic_chip(domain, 0);

	gc->reg_base = reg_base;
	gc->chip_types[0].chip.irq_ack = p2s_mask_irq;
	gc->chip_types[0].chip.irq_mask = p2s_mask_irq;
	gc->chip_types[0].chip.irq_unmask = p2s_unmask_irq;
	gc->chip_types[0].chip.irq_set_type = intc_set_type;

	intc_hw_init(domain);
	set_handle_irq(intc_handle);
	return 0;

err_domain_remove:
	irq_domain_remove(domain);
	return ret;
}
IRQCHIP_DECLARE(p2s_irq, "compas,p2s0927a2-intc", intc_of_init);
